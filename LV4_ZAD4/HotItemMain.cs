﻿using System;
using System.Collections.Generic;
using System.Text;
using LV4_ZAD4;
class HotItemMain
{
    public static void Main()
    {
        RentingConsolePrinter printer = new RentingConsolePrinter();

        List<IRentable> itemsForPrint = new List<IRentable>();
        itemsForPrint.Add(new Book("Iliad"));
        itemsForPrint.Add(new Video("Funny cat videos"));
        itemsForPrint.Add(new HotItem(new Book("Crime and Punishment")));
        itemsForPrint.Add(new HotItem(new Video("BTS - Black Swan")));

        Console.WriteLine("The items you're getting are: ");
        printer.DisplayItems(itemsForPrint);

        Console.WriteLine("You can have all the items for exactly: ");
        printer.PrintTotalPrice(itemsForPrint);

        //Razlike su jedino u samome ispisu, naime HotItems imaju Trending: dok obicni itemi to nemaju kao i sama razlika u sumi iznosa koji
        //se placa. Hot Items imaju pravate bonus koji se nadodaje na standardnu cijenu pa je i suma veca
    }
}
