﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LV4_ZAD4;

namespace LV4_ZAD5
{
	class DiscountedItem : RentableDecorator
	{
		public double discount = 0.2;
		public DiscountedItem(IRentable rentable) : base(rentable) { }
		public override double CalculatePrice()
		{
			return base.CalculatePrice() * (1 - discount);
		}
		public override String Description
		{
			get
			{
				return base.Description + " - now at " + (discount * 100) + "% off!";
			}
		}
	}

}
