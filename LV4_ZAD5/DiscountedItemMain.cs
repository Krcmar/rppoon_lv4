﻿using System.Collections.Generic;
using System;
using LV4_ZAD4;
using LV4_ZAD5;
using System.Text;

class TestDiscountedItem
{
	public static void Main()
	{
		List<IRentable> itemsForPrint = new List<IRentable>();
		itemsForPrint.Add(new Book("Iliad"));
		itemsForPrint.Add(new Video("Funny cat videos"));
		itemsForPrint.Add(new HotItem(new Book("Crime and Punishment")));
		itemsForPrint.Add(new HotItem(new Video("BTS - Black Swan")));
		itemsForPrint.Add(new DiscountedItem(new Book("War and Peace")));
		itemsForPrint.Add(new DiscountedItem(new Video("Panic! At The Disco - High Hopes")));

		Console.WriteLine("The items you're getting are: ");
		printer.DisplayItems(itemsForPrint);

		Console.WriteLine("You can have all the items for exactly: ");
		printer.PrintTotalPrice(itemsForPrint);
	}
}
