﻿using System;
using System.Collections.Generic;
using System.Text;

interface IPasswordValidatorService
{
    bool IsValidPassword(String candidate);
}

