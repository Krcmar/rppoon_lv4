﻿using System;
using System.Collections.Generic;
using System.Text;


interface IEmailValidatorService
{
    bool IsValidAddress(String candidate);
}
