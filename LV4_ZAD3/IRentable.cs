﻿using System;
using System.Collections.Generic;
using System.Text;

interface IRentable
{
    String Description { get; }
    double CalculatePrice();
}
