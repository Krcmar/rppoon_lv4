﻿using System;
using System.Collections.Generic;
using System.Text;
using LV4_ZAD3;

class TestingBookRent
{
    public static void Main()
    {
        RentingConsolePrinter printer = new RentingConsolePrinter();

        List<IRentable> itemsForPrint = new List<IRentable>();
        itemsForPrint.Add(new Book("Iliad"));
        itemsForPrint.Add(new Video("Funny cat videos"));

        Console.WriteLine("The items you're getting are: ");
        printer.DisplayItems(itemsForPrint);


        Console.WriteLine("You can have all the items for exactly: ");
        printer.PrintTotalPrice(itemsForPrint);

    }
}