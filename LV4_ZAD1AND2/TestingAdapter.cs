﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPPOON_LV4;

class TestingAdapter
{
	public static void Main()
	{
		Dataset datasetToReadFile = new Dataset("test.csv");
		IAnalytics analyzer = new Adapter(new Analyzer3rdParty());

		double[] averagePerColumn = analyzer.CalculateAveragePerColumn(datasetToReadFile);
		double[] averagePerRow = analyzer.CalculateAveragePerRow(datasetToReadFile);

		Console.WriteLine("Average value per rows: ");
		foreach (double number in averagePerRow)
			Console.Write(number + " ");
		Console.WriteLine("\n");

		Console.WriteLine("Total average per rows is: " + averagePerRow.Average());

		Console.WriteLine("The average per columns is:");
		foreach (double number in averagePerColumn)
			Console.Write(number + " ");
		Console.WriteLine("\n");
		Console.WriteLine("The total average per columns is: " + averagePerColumn.Average());
	}
}
