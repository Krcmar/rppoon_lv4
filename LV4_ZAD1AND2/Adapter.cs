﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RPPOON_LV4
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> sourceData = dataset.GetData();
            int numberOfRows = sourceData.Count;
            int numberOfColumns = sourceData[0].Count;
            double[][] data = new double[numberOfRows][];

            for (int i = 0; i < numberOfRows; i++)
            {
                data[i] = new double[numberOfRows];
            }

            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < numberOfRows; j++)
                {
                    data[i][j] = sourceData[i][j];
                    data[i][j] = data[j].Average();
                }
            }
            return data;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}