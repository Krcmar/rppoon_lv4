﻿using RPPOON_LV4;
using System;
using System.Collections.Generic;
using System.Text;

interface IAnalytics
{
    double[] CalculateAveragePerColumn(Dataset dataset);
    double[] CalculateAveragePerRow(Dataset dataset);
}